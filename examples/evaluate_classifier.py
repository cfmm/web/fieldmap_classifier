"""
# evaluation different from training stats because of batch normalization
# during training bn layer uses the training batch to normalize
# during evaluation bn layer uses a stored running average of training normalize values
"""
import tensorflow as tf

from train_classifier import classifier_class, data_dir, before_study, client

tf.config.set_visible_devices([], 'GPU')
classifier_class(classifier_class.get_latest_weight_file()).evaluate(client=client, data_dir=data_dir,
                                                                     before_study=before_study)
