import keyring


def store_fieldmaps_password(username, password):
    keyring.set_password('fieldmaps auth_code', username, password)
