from pathlib import Path
from .subject_classifiers import \
    HeadCoilB1SubjectClassifier, \
    HeadCoilB0SubjectClassifier, \
    HeadCoilPtxSubjectClassifier, \
    FieldProbeCoilB1SubjectClassifier, \
    FieldProbeCoilB0SubjectClassifier, \
    FieldProbeCoilPtxSubjectClassifier, \
    BroadB1SubjectClassifier, \
    BroadB0SubjectClassifier, \
    BroadPtxSubjectClassifier, \
    BroadSubjectClassifier

recommended_weights = {
    BroadB1SubjectClassifier.__name__: Path(__file__).parent / 'saved_weights/broad_b1_subject_classifier_2021-08-13T09-47-17.h5',
    BroadB0SubjectClassifier.__name__: Path(__file__).parent / 'saved_weights/broad_b0_subject_classifier_2021-08-13T21-05-29.h5',
    BroadPtxSubjectClassifier.__name__: Path(__file__).parent / 'saved_weights/broad_ptxpulsedesign_subject_classifier_2021-08-13T17-49-43.h5',
    BroadSubjectClassifier.__name__: Path(__file__).parent / 'saved_weights/broad_broad_subject_classifier_2021-08-12T14-57-54.h5',
}