"""
try a simple 3d conv network to start
https://arxiv.org/pdf/2007.13224.pdf

complex:
tensorflow code: https://github.com/NEGU93/cvnn
paper about tf code: https://hal-centralesupelec.archives-ouvertes.fr/hal-02941957/document
survey: https://arxiv.org/pdf/2101.12249.pdf
early paper: https://arxiv.org/pdf/1705.09792.pdf

complex loss:
https://stackoverflow.com/questions/42479024/how-tensorflow-handles-complex-gradient
https://github.com/tensorflow/tensorflow/issues/3348#issuecomment-512101921
"""

from tensorflow import keras
from tensorflow.keras import layers


def get_model(width=64, height=64, depth=25):
    """Build a 3D convolutional neural network model."""

    inputs = keras.Input((width, height, depth, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=(2, 2, 1))(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.BatchNormalization()(x)

    x = layers.GlobalAveragePooling3D()(x)
    x = layers.Dense(units=512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)

    outputs = layers.Dense(units=1, activation="sigmoid")(x)

    # Define the model.
    model = keras.Model(inputs, outputs, name="3dcnn")
    return model


def get_normalized_model(width=64, height=64, depth=25):
    inputs = keras.Input((width, height, depth, 1))
    x = layers.experimental.preprocessing.Normalization()(inputs)
    outputs = get_model(width=width, height=height, depth=depth)(x)
    return keras.Model(inputs, outputs, name='normalized3dcnn')
