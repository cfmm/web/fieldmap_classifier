import numpy as np
from tensorflow import keras


class ModelCheckpoint(keras.callbacks.Callback):
    def __init__(self, filepath, save_weights_only=False, monitor_validation=True):
        self.filepath = filepath
        self.min_loss = np.inf
        self.max_accuracy = 0
        self.save_weights_only = save_weights_only
        self.validation_prefix = 'val_' if monitor_validation else ''

    def on_epoch_end(self, epoch, logs=None):
        loss_key = f'{self.validation_prefix}loss'
        accuracy_key = f'{self.validation_prefix}accuracy'

        save_model = False
        if logs[accuracy_key] > self.max_accuracy:
            print(
                f"{self.validation_prefix}accuracy improved from {self.max_accuracy:.10e} to {logs[accuracy_key]:.10e}, saving model to {self.filepath}")
            self.max_accuracy = logs[accuracy_key]
            self.min_loss = logs[loss_key]
            save_model = True
        elif logs[accuracy_key] == self.max_accuracy and logs[loss_key] < self.min_loss:
            print(
                f"{self.validation_prefix}loss improved from {self.min_loss:.10e} to {logs[loss_key]:.10e}, saving model to {self.filepath}")
            self.min_loss = logs[loss_key]
            save_model = True
        if save_model:
            if self.save_weights_only:
                self.model.save_weights(self.filepath)
            else:
                self.model.save(self.filepath)
        else:
            print(
                f"{self.validation_prefix}accuracy/{self.validation_prefix}loss did not improve from {self.max_accuracy:.10e}/{self.min_loss:.10e}")
