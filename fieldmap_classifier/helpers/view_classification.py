import io

import numpy as np
from fieldmaptools import get_reader
from vidi3d import compare3d


def view_automated_classification(classifier_class, client, weight_file=None, after_study=0, before_study=np.inf):
    weight_file = classifier_class.get_latest_weight_file() if weight_file is None else weight_file
    print(f"using {weight_file}")
    classifier = classifier_class(weight_file)

    coil_info = client.list_all_as_structured_array('coil')
    indexes = np.logical_or.reduce([coil_info['coilname'] == c for c in classifier_class.coils])
    coil_ids = coil_info[indexes]['id'].tolist()
    for imgtype in classifier.filetypes:
        records = client.query_return_structured_array(imgtype, coil_ids=coil_ids)
        for rec in records:
            if rec['study']['id'] <= after_study or rec['study']['id'] >= before_study:
                continue
            img_id = rec['id']
            rv = client.get(imgtype, img_id)
            binary_stream = io.BytesIO(rv.content)
            f = get_reader(binary_stream)
            try:
                pred_class, info = classifier.classify(f)
            except Exception as e:
                print(f'{imgtype} id: {img_id}, {str(e)}')
                continue
            print(f"{imgtype} id: {img_id}, prediction: {pred_class}, info: {info}")
            pred_val = np.max(list(info['class_probabilities'].values()))
            compare3d(
                classifier.get_image(f)[0],
                window_title=f"{imgtype} id: {img_id}, patient name: {f.patientName}",
                subplot_titles=f"{pred_class}, confidence:{pred_val:.2f}"
            )


def view_uncertain_classifications(classifier_class, scan_paths, scan_labels, weight_file=None):
    weight_file = classifier_class.get_latest_weight_file() if weight_file is None else weight_file
    print(f"using {weight_file}")
    classifier = classifier_class(weight_file)

    for filepath, label in zip(scan_paths, scan_labels):
        f = get_reader(filepath)
        try:
            pred_class, info = classifier.classify(f)
        except Exception as e:
            print(filepath, str(e))
            continue
        print(f"{filepath}, truth: {label}, prediction: {pred_class}, info: {info}")
        if pred_class != label:
            compare3d(
                classifier.get_image(f)[0],
                window_title=f"{filepath.name}, patient name: {f.patientName}",
                subplot_titles=f"True label: {label}, confidence: {info['class_probabilities'][label]:.2f}",
            )
